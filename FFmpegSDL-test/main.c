/*
 *	rectangles.c
 *	written by Holmes Futrell
 *	use however you want
 */

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

#include "SDL.h"
#include "SDL_video.h"
#include "SDL_thread.h"
#include <time.h>

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 480

char filePath[] = "/Users/jack/clunet/vicloud/4.repos/transcoder/ffmpeg-examples/ffmpeg-test/srcs/test.mp4";

SDL_Rect rectWindow;
int
randomInt(int min, int max)
{
    return min + rand() % (max - min + 1);
}


void SaveFrame(AVFrame *pFrame, int width, int height, int iFrame) {
    FILE *pFile;
    char szFilename[512];
    int  y;
    
    // Open file
    sprintf(szFilename, "/Users/jack/clunet/vicloud/4.repos/transcoder/ffmpeg-examples/ffmpeg-test/srcs/frame%d.ppm", iFrame);
    pFile=fopen(szFilename, "wb");
    if(pFile==NULL)
        return;
    
    // Write header
    fprintf(pFile, "P6\n%d %d\n255\n", width, height);
    
    // Write pixel data
    for(y=0; y<height; y++)
        fwrite(pFrame->data[0]+y*pFrame->linesize[0], 1, width*3, pFile);
    
    // Close file
    fclose(pFile);
}


void
render(SDL_Renderer *renderer, AVFrame* pFrame, AVCodecContext* pCodecCtx)
{
    static int i;
    printf("\nFrame at: %d", i++);
//    if (i > 990 && i < 995) {
//        SaveFrame(pFrame, pCodecCtx->width, pCodecCtx->height, i);
//    }
    
    
    Uint32 rmask, gmask, bmask, amask;
    
    
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif
    SDL_Surface* surface = SDL_CreateRGBSurfaceFrom(pFrame->data,
                                                    pFrame->width, pFrame->height, 24,
                                                    pFrame->linesize[0],
                                                    rmask, gmask, bmask, amask);
    SDL_Texture * texture= 0;
    /* fill background in with black */
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_NONE);

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);    
    
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surface);    
}


int
main(int argc, char *argv[])
{
#pragma mark Setup SDL
    SDL_Window *window;
    SDL_Renderer *renderer;
    int done;
    SDL_Event event;
    
    /* initialize SDL */
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER) < 0) {
        printf("Could not initialize SDL\n");
        return 1;
    }
    
    /* seed random number generator */
    srand(time(NULL));
    
    /* create window and renderer */
    window =
    SDL_CreateWindow(NULL, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT,
                     SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
    if (!window) {
        printf("Could not initialize Window\n");
        return 1;
    }
    int w,h;
    SDL_GetWindowSize(window, &w, &h);
    rectWindow.x = 0;
    rectWindow.y = 0;
    rectWindow.w = w;
    rectWindow.h = 230;
    
    printf("\nWindow: x=%d, y=%d, w=%d, h=%d", rectWindow.x, rectWindow.y, rectWindow.w, rectWindow.h);
    renderer = SDL_CreateRenderer(window, -1, 0);
    if (!renderer) {
        printf("Could not create renderer\n");
        return 1;
    }

    
#pragma mark Init FFMPEG
    AVFormatContext *pFormatCtx;
    int             i, videoStream;
    AVCodecContext  *pCodecCtx;
    AVCodec         *pCodec;
    AVFrame         *pFrame;
    AVPacket        packet;
    int             frameFinished;
   
    
    av_register_all();
    
    

    
    pFormatCtx = avformat_alloc_context();
    
    // Open video file
    if(avformat_open_input(&pFormatCtx, filePath, NULL, NULL)!=0)
        return -1; // Couldn't open file
    
    // Retrieve stream information
    if(avformat_find_stream_info(pFormatCtx, NULL)<0)
        return -1; // Couldn't find stream information
    
    // Dump information about file onto standard error
    av_dump_format(pFormatCtx, 0, argv[1], 0);
    
    // Find the first video stream
    videoStream=-1;
    for(i=0; i<pFormatCtx->nb_streams; i++)
        if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
            videoStream=i;
            break;
        }
    if(videoStream==-1)
        return -1; // Didn't find a video stream
    
    // Get a pointer to the codec context for the video stream
    pCodecCtx=pFormatCtx->streams[videoStream]->codec;
    
    // Find the decoder for the video stream
    pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec==NULL) {
        fprintf(stderr, "Unsupported codec!\n");
        return -1; // Codec not found
    }
    
    // Open codec
    if(avcodec_open2(pCodecCtx, pCodec, NULL)<0)
        return -1; // Could not open codec
    
    // Allocate video frame
    pFrame=avcodec_alloc_frame();
    
#pragma mark Outputing video frames

    printf("\nFrame pixels format: %d", pCodecCtx->pix_fmt);
    
    static struct SwsContext *img_convert_ctx = NULL;
    uint8_t* buffer;
    AVFrame* pFrameRGB;
    pFrameRGB = avcodec_alloc_frame();
    int numBytes = avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
    
    buffer = (uint8_t *)av_malloc(numBytes*sizeof(uint8_t));
    avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
    
    if (!img_convert_ctx) {
        int w = pCodecCtx->width;
        int h = pCodecCtx->height;
        img_convert_ctx = sws_getContext(w, h, pCodecCtx->pix_fmt,
                                         w, h, PIX_FMT_RGB24,
                                         SWS_BICUBIC, NULL, NULL, NULL);
    }

    done = 0;
    i=0;
    while(av_read_frame(pFormatCtx, &packet)>=0) {
        // Is this a packet from the video stream?
        if(packet.stream_index==videoStream) {
            // Decode video frame
            //avcodec_decode_video(pCodecCtx, pFrame, &frameFinished, packet.data, packet.size);
            avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
            
            if(frameFinished) {
                sws_scale(img_convert_ctx, (const uint8_t * const *)pFrame->data,
                          pFrame->linesize, 0, pCodecCtx->height,
                          pFrameRGB->data, pFrameRGB->linesize);
                

                pFrameRGB->width = pCodecCtx->width;
                pFrameRGB->height = pCodecCtx->width;
                // Free the RGB image
                printf("\nRGBFrame h=%d; w=%d; linesize=%d", pFrameRGB->height, pFrameRGB->width, pFrameRGB->linesize[0]);
                render(renderer, pFrameRGB, pCodecCtx);
            }
        }
        
        // Free the packet that was allocated by av_read_frame
        av_free_packet(&packet);
        
        SDL_PollEvent(&event);
        switch(event.type) {
            case SDL_QUIT:
                done = 1;
                
                break;
            default:
                break;
        }
        
        if (done) {
            break;
        }        
    }
    
    av_free(buffer);
    av_free(pFrameRGB);
    
    // Free the YUV frame
    av_free(pFrame);
    
    // Close the codec
    avcodec_close(pCodecCtx);
    
    // Close the video file
    avformat_close_input(&pFormatCtx);
    
    SDL_Quit();
    return 0;
}
