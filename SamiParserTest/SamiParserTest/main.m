//
//  main.m
//  SamiParserTest
//
//  Created by Jack on 2/14/13.
//  Copyright (c) 2013 Clunet. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SFAppDelegate class]));
    }
}
