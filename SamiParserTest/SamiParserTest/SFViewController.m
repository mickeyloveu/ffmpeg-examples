//
//  SFViewController.m
//  SamiParserTest
//
//  Created by Jack on 2/14/13.
//  Copyright (c) 2013 Clunet. All rights reserved.
//

#import "SFViewController.h"
#import <libxml/tree.h>
#import <libxml/parser.h>
#import <libxml/HTMLtree.h>
#import <libxml/HTMLparser.h>
#import <libxml/xmlstring.h>
#import <libxml/xpath.h>
#import <MediaPlayer/MediaPlayer.h>

NSString* header =
@"<HEAD>"
"<Title>Subtitle file created with Subtitle Converter v2.0</Title>"
"<STYLE TYPE=\"text/css\">"
"<!--"
"P {margin-left:2pt;"
"	margin-right:2pt;"
"	margin-bottom:1pt;"
"	margin-top:1pt;"
" 	text-align:center;"
"  	font-size:20pt;"
"  	font-family:Arial, Sans-serif;"
"  	font-weight:bold;"
"   color:white;}"
".KR {Name:Korean; lang:ko-KR; SAMIType:CC;}"
".EN {Name:English; Lang:en-US; SAMIType:CC;}"
"#STDPrn {Name:Standard Print;}"
"#LargePrn {Name:Large Print; font-size:20pt;}"
"#SmallPrn {Name:Small Print; font-size:10pt;}"
"-->"
"</STYLE>"
"</HEAD>";


NSString* fullRegexStr = @"\\.([a-zA-Z0-9]+)\\s*\\{\\s*(name:.*?;)*?.*?(lang:[-a-zA-Z];)*?.*?\\}";
NSString* complxRegexStr = @"\\.([a-zA-Z0-9]+)(\\s*)(\\{(\\s*)name:(\\s*)([a-zA-Z0-9]+);(\\s*)lang:([a-zA-Z]{2})-([a-zA-Z]{2});(\\s*)\\}*?)";


NSString* simpleRegexStr = @"\\.([a-zA-Z0-9]+)\\s*(\\{.*?\\})";
NSString* properties = @"([a-zA-Z]+):([a-zA-Z-]+);";


const NSString* samiCue = @"<sync start='3195906'>SyncText\n"
"<p class='KR'><font color='#ec14bd'>Translated by 3R</font>\n"
"<font color='#ec14bd'>Translated by $R</font></p></sync>";

//const NSString* incompleteCue = @"<SYNC Start=3195906><P Class=KR>"
//"<font color=\"#ec14bd\">Sync by honeybunny</font><br>"
//"<font color=\"#ec14bd\">Translated by 3R</font>";

const NSString* incompleteCue =
@"<SYNC Start=3065107><P Class=KR>"
"지난 날에 이웃 엘리엇과 이야기해봤는데";

@interface SFViewController ()

@end

@implementation SFViewController


xmlDocPtr
getXmlDoc () {
	xmlDocPtr doc;
    CFStringEncoding cfenc = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
    CFStringRef cfencstr = CFStringConvertEncodingToIANACharSetName(cfenc);
    
    const char *enc = CFStringGetCStringPtr(cfencstr, 0);
    int optionsXml = XML_PARSE_NOERROR | XML_PARSE_NOWARNING | XML_PARSE_RECOVER;
    doc = xmlReadDoc ((xmlChar*)[samiCue UTF8String], NULL, enc, optionsXml);
    return doc;
}

xmlDocPtr
getHtmlDoc () {
	xmlDocPtr doc;
    CFStringEncoding cfenc = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
    CFStringRef cfencstr = CFStringConvertEncodingToIANACharSetName(cfenc);
    
    const char *enc = CFStringGetCStringPtr(cfencstr, 0);

    int optionsHtml = HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING |
                     HTML_PARSE_RECOVER | HTML_PARSE_NODEFDTD;
    
    doc = htmlReadDoc((xmlChar*)[samiCue UTF8String], NULL, enc, optionsHtml);
    return doc;
}

xmlDocPtr
getHtmlDoc1 () {
	xmlDocPtr doc;
    CFStringEncoding cfenc = CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding);
    CFStringRef cfencstr = CFStringConvertEncodingToIANACharSetName(cfenc);
    
    const char *enc = CFStringGetCStringPtr(cfencstr, 0);
    
    int optionsHtml = HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING |
    HTML_PARSE_RECOVER | HTML_PARSE_NODEFDTD;
    
    doc = htmlReadDoc((xmlChar*)[header UTF8String], NULL, enc, optionsHtml);
    return doc;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//------------------------------------------------------------------------------
- (IBAction)samiparse:(id)sender {
    
    xmlDocPtr doc = getHtmlDoc();
   
    FILE* dumfile = fopen("/Users/jack/clunet/vicloud/0.tmp/dumphtml", "w");
    xmlDocDump(dumfile, doc);
    fclose(dumfile);
    
        
    
   	xmlFreeDoc(doc);
}


//------------------------------------------------------------------------------
void
parseStory (xmlDocPtr doc, xmlNodePtr cur) {
    
	xmlChar *key;
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"font"))) {
		    key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		    printf("keyword: %s\n", key);
		    xmlFree(key);
 	    }
        cur = cur->next;
	}
    return;
}


- (IBAction)parseXml:(id)sender {
    xmlDocPtr doc;
	xmlNodePtr cur;
    
    doc = getXmlDoc();
    
	if (doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		return;
	}
	
	cur = xmlDocGetRootElement(doc);
	
	if (cur == NULL) {
		fprintf(stderr,"empty document\n");
		xmlFreeDoc(doc);
		return;
	}
	
	if (xmlStrcmp(cur->name, (const xmlChar *) "sync")) {
		fprintf(stderr,"document of the wrong type, root node != story");
		xmlFreeDoc(doc);
		return;
	}
	
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"p"))){
			parseStory (doc, cur);
		}
        
        cur = cur->next;
	}
	
	xmlFreeDoc(doc);
}


//------------------------- XPATH TEST -----------------------------------------
xmlXPathObjectPtr
getnodeset (xmlDocPtr doc, xmlChar *xpath){
	
	xmlXPathContextPtr context;
	xmlXPathObjectPtr result;
    
	context = xmlXPathNewContext(doc);
	if (context == NULL) {
		printf("Error in xmlXPathNewContext\n");
		return NULL;
	}
	result = xmlXPathEvalExpression(xpath, context);
	xmlXPathFreeContext(context);
	if (result == NULL) {
		printf("Error in xmlXPathEvalExpression\n");
		return NULL;
	}
	if(xmlXPathNodeSetIsEmpty(result->nodesetval)){
		xmlXPathFreeObject(result);
        printf("No result\n");
		return NULL;
	}
	return result;
}

- (IBAction)xpathRetrieve:(id)sender
{
    xmlDocPtr doc;
	xmlChar *xpath = (xmlChar*) "/html/body/sync/p/font";
	xmlNodeSetPtr nodeset;
	xmlXPathObjectPtr result;
	int i;
	xmlChar *keyword;
    
	doc = getHtmlDoc();
    
    FILE* dumfile = fopen("/Users/jack/clunet/vicloud/0.tmp/xpathdump", "w");
    xmlDocDump(dumfile, doc);
    fclose(dumfile);

	result = getnodeset (doc, xpath);
	if (result) {
		nodeset = result->nodesetval;
		for (i=0; i < nodeset->nodeNr; i++) {
			keyword = xmlNodeListGetString(doc, nodeset->nodeTab[i]->xmlChildrenNode, 1);
            printf("keyword: %s\n", keyword);
            xmlFree(keyword);
		}
		xmlXPathFreeObject (result);
	}
	xmlFreeDoc(doc);
	xmlCleanupParser();
    
}

//--------------------------GET ATTRIBUTE TEST ---------------------------------
void
getReference (xmlDocPtr doc, xmlNodePtr cur) {
    
	xmlChar *uri;
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"p"))) {
		    uri = xmlGetProp(cur, (const xmlChar *) "class");
		    printf("uri: %s\n", uri);
		    xmlFree(uri);
	    }
	    cur = cur->next;
	}
	return;
}

- (IBAction)getAttribute:(id)sender {
    xmlDocPtr doc;
	xmlNodePtr cur;
    
	doc = getXmlDoc();
	
	if (doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		return;
	}
	
	cur = xmlDocGetRootElement(doc);
	
	if (cur == NULL) {
		fprintf(stderr,"empty document\n");
		xmlFreeDoc(doc);
		return;
	}
	
	if (xmlStrcmp(cur->name, (const xmlChar *) "sync")) {
		fprintf(stderr,"document of the wrong type, root node != story");
		xmlFreeDoc(doc);
		return;
	}else{
        xmlChar* start;
        start = xmlGetProp(cur, (const xmlChar *) "start");
        printf("start: %s\n", start);
        xmlFree(start);
    }
	
	getReference (doc, cur);
    
	xmlFreeDoc(doc);
	return;
}

//--------------------------REGULAR EXPRESSION TEST-----------------------------
- (IBAction)regExp:(id)sender {
    NSString* classID = nil;
    NSString* classInfo = nil;
    NSError* error=nil;
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:simpleRegexStr
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    
    NSLog(@"ERROR: %@",error);
    NSArray* regResults = [regex
                           matchesInString:header
                           options:0
                           range:NSMakeRange(0, [header length])];
    
    for (NSTextCheckingResult *result in regResults) {
        NSString *classTag = [header substringWithRange:result.range];
        NSLog(@"Whole range: %@", classTag);
        
        NSLog(@"Number of range: %d", result.numberOfRanges);
        for (int i = 1; i < result.numberOfRanges;++i) {
            NSLog(@"At index %d: %@", i, [header substringWithRange:[result rangeAtIndex:i]]);
        }
        
        classID = [header substringWithRange:[result rangeAtIndex:1]];
        classInfo = [header substringWithRange:[result rangeAtIndex:2]] ;
        NSRegularExpression *proReg = [NSRegularExpression
                                      regularExpressionWithPattern:properties
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        
        NSArray* proResults = [proReg
                               matchesInString:classInfo
                               options:0
                               range:NSMakeRange(0, [classInfo length])];
        
        for (NSTextCheckingResult* proResult in proResults) {
            NSLog(@"Key/Val: %@:%@",
                  [classInfo substringWithRange:[proResult rangeAtIndex:1]],
                  [classInfo substringWithRange:[proResult rangeAtIndex:2]]);
        }
    }
}
//--------------------------------TRAVERLER-------------------------------------
static void
print_element_names(xmlNode * a_node)
{
    xmlNode *cur_node = NULL;
    
    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            printf("node type: Element, name: %s\n", cur_node->name);
        }
        
        print_element_names(cur_node->children);
    }
}

- (IBAction)nodetraver:(id)sender {
    xmlDocPtr pDoc = getHtmlDoc();
    xmlNodePtr root_element = xmlDocGetRootElement(pDoc);
    print_element_names(root_element);
   
    xmlChar* rootContent = xmlNodeGetContent(root_element);
    printf("\nContent of root node:\n%s", rootContent);
    
    xmlFree(rootContent);
    xmlFreeDoc(pDoc);
}


//--------------------------------Play movie -----------------------------------

- (IBAction)playmoview:(id)sender {
    NSString* moviePath = [[NSBundle mainBundle] pathForResource:@"howimet"
                                                          ofType:@"mp4"];
    
    NSURL* url = [NSURL fileURLWithPath:moviePath];
    
    MPMoviePlayerViewController *theMovie = [[MPMoviePlayerViewController alloc]
                                             initWithContentURL: url];
    
    [self presentMoviePlayerViewControllerAnimated:theMovie];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(myMovieFinishedCallback:)
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:theMovie];
}
-(void)myMovieFinishedCallback:(NSNotification*)aNotification {
    [self dismissMoviePlayerViewControllerAnimated];
    MPMoviePlayerController* theMovie = [aNotification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:theMovie];
}
@end
