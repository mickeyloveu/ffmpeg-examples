//
//  SFViewController.h
//  SamiParserTest
//
//  Created by Jack on 2/14/13.
//  Copyright (c) 2013 Clunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFViewController : UIViewController
- (IBAction)samiparse:(id)sender;
- (IBAction)parseXml:(id)sender;
- (IBAction)xpathRetrieve:(id)sender;
- (IBAction)getAttribute:(id)sender;
- (IBAction)regExp:(id)sender;
- (IBAction)nodetraver:(id)sender;
- (IBAction)playmoview:(id)sender;

@end
