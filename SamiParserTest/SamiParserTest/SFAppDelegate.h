//
//  SFAppDelegate.h
//  SamiParserTest
//
//  Created by Jack on 2/14/13.
//  Copyright (c) 2013 Clunet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
